<?php declare(strict_types=1);

namespace AmethystCreative\MagentoPOS\Console\Command;

use Magento\Framework\App\Area;
use Magento\Store\Model\Store;
use Magento\Framework\App\State;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Store\Model\App\Emulation;
use AmethystCreative\MagentoPOS\Model\Pos;
use AmethystCreative\MagentoPOS\Model\PosFactory;
use AmethystCreative\MagentoPOS\Model\ResourceModel\Pos as PosResource;

class AddNewPos extends Command
{

    /** @var State */
    protected $state;

    /** @var Emulation */
    protected $emulation;

    /** @var PosFactory */
    protected $posFactory;

    /** @var PosResource */
    protected $posResource;

    /**
     * AddNewPos constructor.
     * @param State $state A Magento app State instance
     * @param Emulation $emulation
     * @param PosFactory $posFactory
     * @param PosResource $posResource
     */
    public function __construct(
        State $state,
        Emulation $emulation,
        PosFactory $posFactory,
        PosResource $posResource
    ) {
        $this->state = $state;
        $this->emulation = $emulation;
        $this->posFactory = $posFactory;
        $this->posResource = $posResource;
        try {
            $this->state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {

        }
        parent::__construct();
    }

    /**
     * Configures arguments and display options for this command.
     */
    protected function configure()
    {
        $this->setName('magexo:pos:add');
        $this->setDescription("Adds new point of sale into database! Sample usage: bin/magento magexo:pos:add 'Point of Sale 300' 'rue de camus 46' 1");
        $this->addArgument(
            'name',
            InputArgument::REQUIRED,
            'POS Name'
        );
        $this->addArgument(
            'address',
            InputArgument::REQUIRED,
            'POS Address'
        );
        $this->addArgument(
            'is_available',
            InputArgument::REQUIRED,
            'Is available'
        );
        parent::configure();
    }

    /**
     * Executes the command to add a new pos to the database.
     *
     * @param InputInterface $input An input instance
     * @param OutputInterface $output An output instance
     *
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if(!is_string($input->getArgument('name'))) {
            $output->writeln('<error>Name argument should be a string.</error>');
            return;
        }

        if(!is_string($input->getArgument('address'))) {
            $output->writeln('<error>Address argument should be a string.</error>');
            return;
        }

        if(!is_numeric($input->getArgument('is_available')) || $input->getArgument('is_available') > 1) {
            $output->writeln('<error>Is_available argument should be numeric 0 or 1.</error>');
            return;
        }

        $posName = $input->getArgument('name');
        $posAddress = $input->getArgument('address');
        $posIsAvailable = (bool)$input->getArgument('is_available');

        /**
         * Start emulating admin environment
         */
        $this->emulation->startEnvironmentEmulation(Store::DEFAULT_STORE_ID, Area::AREA_ADMINHTML, true);

        try {
            /** @var Pos $pos */
            $pos = $this->posFactory->create();
            $pos->setData('name', $posName);
            $pos->setData('address', $posAddress);
            $pos->setData('is_available', $posIsAvailable);

            $this->posResource->save($pos);
        } catch (\Exception $e) {
            $output->writeln('<error>Something went wrong while saving item.</error>');
        }

        $output->writeln('<info>Created new POS successfully!</info>');

        $this->emulation->stopEnvironmentEmulation();

    }

}
