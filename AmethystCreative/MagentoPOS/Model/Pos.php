<?php declare(strict_types=1);

namespace AmethystCreative\MagentoPOS\Model;

use Magento\Framework\Model\AbstractModel;

class Pos extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(ResourceModel\Pos::class);
    }
}
