<?php declare(strict_types=1);

namespace AmethystCreative\MagentoPOS\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Pos extends AbstractDb
{
    /** @var string Main table name */
    const MAIN_TABLE = 'amethystcreative_magentopos_pos';

    /** @var string Main table primary key field name */
    const ID_FIELD_NAME = 'pos_id';

    protected function _construct()
    {
        $this->_init(self::MAIN_TABLE, self::ID_FIELD_NAME);
    }
}
