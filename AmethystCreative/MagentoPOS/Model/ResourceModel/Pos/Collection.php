<?php declare(strict_types=1);

namespace AmethystCreative\MagentoPOS\Model\ResourceModel\Pos;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use AmethystCreative\MagentoPOS\Model\Pos;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(Pos::class, \AmethystCreative\MagentoPOS\Model\ResourceModel\Pos::class);
    }
}
