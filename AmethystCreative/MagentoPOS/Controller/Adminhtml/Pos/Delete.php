<?php declare(strict_types=1);

namespace AmethystCreative\MagentoPOS\Controller\Adminhtml\Pos;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;

use AmethystCreative\MagentoPOS\Model\Pos;
use AmethystCreative\MagentoPOS\Model\PosFactory;
use AmethystCreative\MagentoPOS\Model\ResourceModel\Pos as PosResource;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;

class Delete extends Action implements HttpGetActionInterface
{
    const ADMIN_RESOURCE = 'AmethystCreative_MagentoPOS::pos_delete';

    /** @var PosFactory */
    protected $posFactory;

    /** @var PosResource */
    protected $posResource;

    /**
     * Delete constructor.
     * @param Context $context
     * @param PosFactory $posFactory
     * @param PosResource $posResource
     */
    public function __construct(
        Context $context,
        PosFactory $posFactory,
        PosResource $posResource
    ) {
        $this->posFactory = $posFactory;
        $this->posResource = $posResource;
        parent::__construct($context);
    }

    /**
     * @return Redirect
     */
    public function execute(): Redirect
    {
        try {
            $id = $this->getRequest()->getParam('pos_id');
            /** @var Pos $pos */
            $pos = $this->posFactory->create();
            $this->posResource->load($pos, $id);
            if ($pos->getPosId()) {
                $this->posResource->delete($pos);
                $this->messageManager->addSuccessMessage(__('The record has been deleted.'));
            } else {
                $this->messageManager->addErrorMessage(__('The record does not exist.'));
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        /** @var Redirect $redirect */
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $redirect->setPath('*/*');
    }
}
