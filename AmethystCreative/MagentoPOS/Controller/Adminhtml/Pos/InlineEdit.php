<?php declare(strict_types=1);

namespace AmethystCreative\MagentoPOS\Controller\Adminhtml\Pos;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use AmethystCreative\MagentoPOS\Model\Pos;
use AmethystCreative\MagentoPOS\Model\PosFactory;
use AmethystCreative\MagentoPOS\Model\ResourceModel\Pos as PosResource;

class InlineEdit extends Action implements HttpPostActionInterface
{
    const ADMIN_RESOURCE = 'AmethystCreative_MagentoPOS::pos_save';

    /** @var JsonFactory */
    protected $jsonFactory;

    /** @var PosFactory */
    protected $posFactory;

    /** @var PosResource */
    protected $posResource;

    /**
     * InlineEdit constructor.
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param PosFactory $posFactory
     * @param PosResource $posResource
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        PosFactory $posFactory,
        PosResource $posResource
    ) {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
        $this->posFactory = $posFactory;
        $this->posResource = $posResource;
    }


    public function execute()
    {
        $json = $this->jsonFactory->create();
        $messages = [];
        $error = false;
        $isAjax = $this->getRequest()->getParam('isAjax', false);
        $items = $this->getRequest()->getParam('items', []);

        if (!$isAjax || !count($items)) {
            $messages[] = __('Please correct the data sent.');
            $error = true;
        }

        if (!$error) {
            foreach ($items as $item) {
                $id = $item['pos_id'];
                try {
                    /** @var Pos $pos */
                    $pos = $this->posFactory->create();
                    $this->posResource->load($pos, $id);
                    $pos->setData(array_merge($pos->getData(), $item));
                    $this->posResource->save($pos);
                } catch (\Exception $e) {
                    $messages[] = __("Something went wrong while saving item $id");
                    $error = true;
                }
            }
        }

        return $json->setData([
            'messages' => $messages,
            'error' => $error,
        ]);
    }
}
