<?php declare(strict_types=1);

namespace AmethystCreative\MagentoPOS\Setup\Patch\Data;

use AmethystCreative\MagentoPOS\Model\ResourceModel\Pos;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchInterface;

class InitialPos implements DataPatchInterface
{
    /** @var ModuleDataSetupInterface */
    protected $moduleDataSetup;

    /** @var ResourceConnection */
    protected $resource;

    /**
     * InitialPos constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param ResourceConnection $resource
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        ResourceConnection $resource
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->resource = $resource;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

    public function apply(): self
    {
        $connection = $this->resource->getConnection();
        $data = [
            [
                'name' => 'Point Of Sale 1',
                'address' => 'Palace Hall Store',
                'is_available' => 1,
            ],
            [
                'name' => 'Point Of Sale 2',
                'address' => 'New York Store',
                'is_available' => 1,
            ],
            [
                'name' => 'Point Of Sale 100',
                'address' => 'Paris Store',
                'is_available' => 0,
            ],
        ];
        $connection->insertMultiple(Pos::MAIN_TABLE, $data);

        return $this;
    }
}
